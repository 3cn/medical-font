# Medical-Font

This is a font project.  The font is illustrated in a single adobe illustrator file.  Svgs of each glyph are exported to /svg with the filename matching the glyph name.  The font is created with [icomoon](https://icomoon.io/).  It is provided in multiple formats with a demo page and the stylesheets for the font as well as `selection.json` which is the icomoon export configuration file.  Icomoon provided some great export options.  

icomoon.json is the icomoon project file.  Simply upload this to the [icomoon app](https://icomoon.io/app) in order to start working on the font.  Don't forget to update the file with any changes that you make.  Your icomoon project only lives in the cache on your browser, so do not worry about permissions unless you are a premium user.

medical-font is used in html like this `<i class="ac-icon ac-meds"></i>`

`bower install --save [this repo]` in your project to install this as a bower dependency.

Once you have cloned the repo you can check out the [demo](icomoon/demo.html).

You should still be able to use fa-2x, 3x and 4x multipliers to scale your icons if you are using font awesome.  Otherwise you might want to include convenience rules like `.ac-2x { font-size:2em; }` in order to conveniently scale your icons.